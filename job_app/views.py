# Core import
import os
import asyncio
# from threading import Thread

# Third party import
from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

# App level import
from .forms import UploadDocs
from .read_data import process
from recruiter_dashboard import settings


MESSAGES = {}
MESSAGES['empty_form'] = 'Please attach atlease one file with .csv or .xls ext'
MESSAGES['error_processing_file'] = 'File uploaded but not processed.'
MESSAGES['upload_succesfull'] = 'All files uploaded successfully.'


def dashboard(request):
    form = UploadDocs()
    return render(request, 'job_app/dashboard.html', {'form': form})


@csrf_exempt
def upload_file(request):
    """File upload controller"""
    if request.method == 'POST':

        form = UploadDocs(request.POST, request.FILES)
        if form.is_valid():
            try:
                files = request.FILES.getlist('file')
                file_path = upload_handler(files)
                process_file(file_path)
                context = {'message': MESSAGES.get('upload_succesfull')}
                return JsonResponse(context)
            except:
                context = {'message': MESSAGES.get('error_processing_file')}
                return JsonResponse(context)
        else:
            context = {'message': MESSAGES.get('empty_form')}
            return JsonResponse(context)


def upload_handler(files):
    """Save the uploaded file"""
    file_path = []
    if files:
        try:
            for file in files[:10]:
                file_name = file.name
                path_to_upload = os.path.join(settings.MEDIA_ROOT, file_name)
                if not os.path.exists(settings.MEDIA_ROOT):
                    os.mkdir('media')
                file_path.append(path_to_upload)
                path_to_upload = open(path_to_upload, 'wb+')
                file_data = file.chunks()
                for data in file_data:
                    path_to_upload.write(data)
        except:
            pass

    return file_path


def process_file(files_list):
    """
    Function to call a coroutine which in turns
    process the data concurrently.
    """
    asyncio.run(process.calculate_file_data(files_list))
