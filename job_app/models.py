from django.db import models


class Job(models.Model):
    """
    Demo purpose
    Not implemented
    """

    job_id = models.IntegerField(max_length=100)
    role = models.CharField(blank=True, null=True, max_length=100)
    level = models.CharField(blank=True, null=True, max_length=100)
    primary_skills = models.CharField(blank=True, null=True, max_length=100)
    description = models.CharField(blank=True, null=True, max_length=100)

    def __str__(self):
        """Instance description"""
        return '{} {} with skills - {}'.format(
            self.level, self.role, self.primary_skills)

    class Meta:
        ordering = ['role', 'job_id']
        verbose_name_plural = 'Job List'
