from django.urls import path

from .views import dashboard, upload_file


urlpatterns = [
    path('', dashboard, name='dashboard'),
    path('upload-file/', upload_file, name='upload_file')
]
