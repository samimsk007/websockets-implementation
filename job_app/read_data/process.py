# Core import
import asyncio
# from dateutil import parser
# from datetime import datetime

# Third-party import
import pandas as pd
import websockets


async def calculate_file_data(file_list):
    """
    Compute the data of all the files
    concurrently using the coroutine -
    calculate_single_file.
    """
    await asyncio.gather(*[calculate_single_file(
        file_name) for file_name in file_list])


async def calculate_single_file(filename):
    """
    1. Get the file path
    2. check the extension.
    3. If not correct extension return.
    4. create a dataframe depending upon extension name using the file path.
    5. If correct entension call a coroutine call
       calculate stats which computes the result
    """
    if not isinstance(filename, str):
        return
    if ('.csv' not in filename) and ('.xlsx' not in filename):
        return
    task1 = asyncio.create_task(
        get_stats(filename))
    await task1

async def send(data):
    """
    Take the input, creates a connection to socket server
    and send the data to the server
    """
    async with websockets.connect(
            'ws://localhost:8765') as websocket:
        try:
            message = list()
            for field, value in data.items():
                field_message = str(field) + ':  ' + str(value)
                message.append(field_message)

            await websocket.send(', '.join(message))
            print('Sent....')
        except:
            pass


async def send_calulated_data_client(data):
    task1 = asyncio.create_task(
        send(data))
    await task1

async def get_stats(filename):
    """
    Get the stats and send the response to socket client.
    (The socket client send the data to server and the server
     sends the data to web browser.)
    """
    result = {}
    try:
        if '.xls' in filename:
            original_df = pd.read_excel(filename, index_col=1)
        elif '.csv' in filename:
            original_df = pd.read_csv(filename, index_col=1)
        else:
            return {}

        df = original_df.groupby(original_df.index).first().head(10000)

        result['Filename'] = filename.split('/')[-1]
        result['Total Rows'] = original_df.shape[0]
        result['Valid Rows'] = df.shape[0]
        result['Row ignored'] = original_df.shape[0] - df.shape[0]
        result['Job description created'] = df.shape[0]

        task = asyncio.create_task(send_calulated_data_client(result))
        await task

    except:
        result['Filename'] = filename.split('/')[-1]
        result['Error'] = 'Corrupted data format.'
        task = asyncio.create_task(send_calulated_data_client(result))
        await task
