Instruction to run the app

1. Create a virtualenv
2. Install the dependencies(requiremets.txt)
3. Run the django app (python manage.py runserver)
4. Navigate inside socket_server folder
5. Run the socket server (python server.py)
6. Open browser - 'http://localhost:8000'

